#!/usr/bin/env python3

# Copyright © 2024 Red Hat, Inc.

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from unittest.mock import patch, MagicMock

import pytest
import re

from mr_label_maker.project import GitLabProject


@patch("gitlab.gitlab.Gitlab")  # So we can't ever instantiate the real one
class TestProject:
    @pytest.mark.parametrize(
        "src", [None, "", "foo", "baz", ["foo", "bar", "baz", "bat"]]
    )
    def test_add_labels(self, gitlab, src):
        dst = set()
        if not src:
            with pytest.raises(ValueError):
                GitLabProject.add_labels(dst, src)
            return

        GitLabProject.add_labels(dst, src)
        if isinstance(src, str):
            assert dst == set([src])
        else:
            assert dst == set(src)

        dst = set(("foo", "bar"))
        GitLabProject.add_labels(dst, src)
        if src == "foo":
            assert sorted(dst) == sorted(("foo", "bar"))
        elif src == "baz":
            assert sorted(dst) == sorted(("foo", "bar", "baz"))
        elif src == ["foo", "bar", "baz", "bat"]:
            assert sorted(dst) == sorted(("foo", "bar", "baz", "bat"))
        else:
            assert False, f"Unhandled src argument: {src}"

    def test_match_starts_with(self, gitlab):
        def underscorify(txt):
            return f"_{txt}"

        mymap = {
            "foo": "one",
            "f": ["two", "three"],
            "_foo": ["no1", "no2"],
            "baz": "no3",
            "bar": "no4",
        }

        text = "_foobar baz"
        labels = set(["foo", "bar"])
        assert (
            GitLabProject.match_starts_with(labels, underscorify, mymap, text) is True
        )

        for expected in ["foo", "bar", "one", "two", "three"]:
            assert expected in labels

        for label in labels:
            assert not label.startswith("no")

    def test_match_contains(self, gitlab):
        def underscorify(txt):
            return f"_{txt}"

        mymap = {
            "foo": "one",
            "f": ["two", "three"],
            "_foo": ["no1", "no2"],
            "baz": "no3",
            "bar": "no4",
        }

        text = "bar _foobar baz"
        labels = set(["foo", "bar"])
        assert GitLabProject.match_contains(labels, underscorify, mymap, text) is True

        for expected in ["foo", "bar", "one", "two", "three"]:
            assert expected in labels

        for label in labels:
            assert not label.startswith("no")

    def test_match_re(self, gitlab):
        mymap = {
            re.compile(r"foo"): "one",
            re.compile(r"f+"): ["two", "three"],
            re.compile(r"_fx?o+"): ["four", "five"],
            re.compile(r"ba(z|r)"): "six",
            re.compile(r"bar{2}"): "no4",
        }

        text = "bar _foobar baz"
        labels = set(["foo", "bar"])
        assert GitLabProject.match_re(labels, mymap, text) is True

        for expected in ["foo", "bar", "one", "two", "three", "four", "five", "six"]:
            assert expected in labels

        for label in labels:
            assert not label.startswith("no")

    @pytest.mark.parametrize(
        "title, expected",
        (
            ("foo: no: bar", ["f1", "f2"]),
            ("no: foo: bar", ["f1", "f2"]),
            ("no: foo: bar:", ["f1", "f2"]),
            ("no: foo: bar: ", ["f1", "f2", "b1", "b2"]),
            ("no: [foo] bar: ", ["f1", "f2", "b1", "b2"]),
            ("no: [foo] [bar]", ["f1", "f2", "b1", "b2"]),
            ("no: [foo] [bar] codename", ["f1", "f2", "b1", "b2", "cn"]),
        ),
    )
    def test_eval_title(self, gitlab, title, expected):
        config = MagicMock()
        config.issues.areas = {
            "foo": ["f1", "f2"],
            "bar": ["b1", "b2"],
            "nope": ["no1"],
        }
        config.issues.title_codenames = {
            "codename": ["cn"],
            "nopename": ["no2"],
        }
        proj = GitLabProject(config, token="abcde")

        labels = set(["foo", "bar"])
        # eval_title checks for:
        #   starts with "name:"
        #   contains " name:" (note the space)
        #   contains " [name]"
        #   contains "codename"
        proj.eval_title(labels, title, config.issues)

        # labels never get removed
        assert "foo" in labels
        assert "bar" in labels
        for e in expected:
            assert e in labels

        for all_labels in list(config.areas.values()) + list(
            config.title_codenames.values()
        ):
            for l in filter(lambda x: x not in expected, all_labels):
                assert l not in labels

    @pytest.mark.parametrize(
        "desc, expected",
        (
            ("this is some random text", []),
            ("this text contains a codename, yay", ["cn"]),
            ("this text does not contain a code_name, no", []),
            ("this text contains a [codename], yay", ["cn"]),
            ("this text contains a hiddencodenameinaword, yay", ["cn"]),
        ),
    )
    def test_eval_description(self, gitlab, desc, expected):
        config = MagicMock()
        config.issues.desc_codenames = {
            "codename": ["cn"],
            "nopename": ["no2"],
        }
        proj = GitLabProject(config, token="abcde")

        labels = set(["foo", "bar"])
        # eval_description just checks for substrings
        proj.eval_description(labels, desc, config.issues)

        # labels never get removed
        assert "foo" in labels
        assert "bar" in labels
        for e in expected:
            assert e in labels

        for all_labels in config.desc_codenames.values():
            for l in filter(lambda x: x not in expected, all_labels):
                assert l not in labels

    @pytest.mark.parametrize(
        "path, expected",
        (
            ("/foo/bar/baz", ["f1", "b1"]),
            ("baz/foo", ["f1", "b3"]),
            ("baz/foo/bar", ["f1", "f2", "b2", "b3"]),
            ("baz/foobar", ["f1", "f2", "b2", "b3"]),
            ("xbaz/foobar", ["f1", "f2", "b2"]),
        ),
    )
    def test_eval_paths(self, gitlab, path, expected):
        config = MagicMock()
        config.mrs.paths = {
            re.compile(r"foo"): ["f1"],
            re.compile(r"bar"): ["b1"],
            re.compile(r"foo/?bar"): ["f2", "b2"],
            re.compile(r"^baz"): ["b3"],
        }
        proj = GitLabProject(config, token="abcde")

        labels = set(["foo", "bar"])
        # eval_path just checks for regex search
        proj.eval_path(labels, path, config.mrs)

        # labels never get removed
        assert "foo" in labels
        assert "bar" in labels
        for e in expected:
            assert e in labels

        for all_labels in config.desc_codenames.values():
            for l in filter(lambda x: x not in expected, all_labels):
                assert l not in labels

    def test_connect(self, gitlab):
        config = MagicMock()
        instance = MagicMock()
        gitlab.return_value = instance  # override constructor

        proj = GitLabProject(config, token="abcde")
        proj.connect()
        gitlab.assert_called_with(config.url, "abcde")
        instance.projects.get.assert_called_with(config.gitlab_id)

    @pytest.mark.parametrize("which", ("issue", "mr"))
    @pytest.mark.parametrize("title_prefix", ("wip: ", "draft: ", ""))
    @pytest.mark.parametrize("dry_run", (True, False))
    @pytest.mark.parametrize("ignore_history", (True, False))
    @pytest.mark.parametrize("expect_labels", (True, False))
    def test_process_issue(
        self, gitlab, which, title_prefix, dry_run, ignore_history, expect_labels
    ):
        config = MagicMock()
        config.issues.areas = {
            "foo": "f1",
            "bar": "b1",
        }
        config.issues.title_codenames = {
            "my": "my",
            "your": "your",
        }
        config.issues.desc_codenames = {
            "stout": "stout",
            "lager": "lager",
        }
        config.mrs.areas = {
            "foo": "f2",
            "bar": "b2",
        }
        config.mrs.title_codenames = {
            "my": "my2",
            "your": "your2",
        }
        config.mrs.desc_codenames = {
            "stout": "stout2",
            "lager": "lager2",
        }
        config.mrs.paths = {
            re.compile(r"^same"): "same",
            re.compile(r"new"): "new",
            re.compile(r"notnew"): "notnew",
        }

        issue = MagicMock()
        if expect_labels:
            issue.title = f"{title_prefix}foo: this is my title"
            issue.description = "I'm a little issue, short and stout"
        else:
            issue.title = f"{title_prefix}baz: this is their title"
            issue.description = "I expect the mr to just bail out"

        issue.labels = [] if not ignore_history else ["foo", "bar"]
        issue.attributes = {"web_url": "https://example.com"}
        if which == "mr":
            issue.attributes["sha"] = "12346beef"
            if expect_labels:
                issue.changes.return_value = {
                    "changes": [
                        {"old_path": "someoldfile.c", "new_path": "somenewfile.c"},
                        {"old_path": "samefile.c", "new_path": "samefile.c"},
                    ]
                }
            else:
                issue.changes.return_value = {
                    "changes": [
                        {"old_path": "otherfile.c", "new_path": "thatfile.c"},
                        {"old_path": "unchanged.c", "new_path": "unchanged.c"},
                    ]
                }

        proj = GitLabProject(
            config, token="abcde", dry_run=dry_run, ignore_label_history=ignore_history
        )
        proj.process_issue_or_mr(issue, which == "issue")

        if title_prefix or dry_run or not expect_labels:
            issue.save.assert_not_called()
            return

        issue.save.assert_called()
        if which == "issue":
            expected = ["f1", "my", "stout"]
        else:
            expected = ["f2", "my2", "stout2"]
        for e in expected:
            assert e in issue.labels

        unexpected = ["b1", "your", "lager", "notnew"]
        unexpected += ["b2", "your2", "lager2", "notnew2"]
        for u in unexpected:
            assert u not in issue.labels

        if ignore_history:
            assert "foo" in issue.labels
            assert "bar" in issue.labels
        else:
            assert "foo" not in issue.labels
            assert "bar" not in issue.labels

        if which == "issue":
            issue.changes.assert_not_called()
        elif which == "mr":
            expected = ["same", "new"]
            for e in expected:
                assert e in issue.labels
